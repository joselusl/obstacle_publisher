
#ifndef _OBSTACLE_PUBLISHER_H_
#define _OBSTACLE_PUBLISHER_H_



#include <string>
#include <fstream>
#include <sstream>

#include <iostream>

#include <pugixml/pugixml.hpp>

#include <Eigen/Dense>

#include <quaternion_algebra/quaternion_algebra.h>

#include <ros/ros.h>

#include <visualization_msgs/MarkerArray.h>

#include "environment_msgs/Object2dArray.h"




////////////////////////////////////
/// \brief The ObstaclePublisher3d class
///////////////////////////////////
class ObstaclePublisher3d
{
public:
    ObstaclePublisher3d(int argc,char **argv);


protected:
    ros::NodeHandle* nh_;

protected:
    ros::Rate* rate_;
    double rate_val_;


protected:
    void readParameters();

    int readConfigFile();


protected:
    std::string obstacles_file_;


protected:
    std::string world_ref_frame_;


protected:
    std::string obstacles_pub_topic_name_;
    ros::Publisher obstacles_pub_;
    visualization_msgs::MarkerArray obstacles_pub_msg_;

protected:
    int publish();


public:
    int open();

public:
    int run();

public:
    void sleep();


};





////////////////////////////////////
/// \brief The ObstaclePublisher2d class
///////////////////////////////////
class ObstaclePublisher2d
{
public:
    ObstaclePublisher2d(int argc,char **argv);


protected:
    ros::NodeHandle* nh_;

protected:
    ros::Rate* rate_;
    double rate_val_;


protected:
    void readParameters();

    int readConfigFile();


protected:
    std::string obstacles_file_;

protected:
    std::string world_ref_frame_;


protected:
    std::string obstacles_3d_pub_topic_name_;
    ros::Publisher obstacles_3d_pub_;
    visualization_msgs::MarkerArray obstacles_3d_pub_msg_;

protected:
    std::string obstacles_2d_pub_topic_name_;
    ros::Publisher obstacles_2d_pub_;
    environment_msgs::Object2dArray obstacles_2d_pub_msg_;


protected:
    int publish();


public:
    int open();

public:
    int run();

public:
    void sleep();


};





#endif
