
#include "obstacle_publisher/obstacle_publisher.h"



ObstaclePublisher3d::ObstaclePublisher3d(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    nh_=new ros::NodeHandle();

    // info
    ROS_INFO("Starting %s",ros::this_node::getName().c_str());

    // read parameters
    readParameters();

    // post initializations
    rate_=new ros::Rate(rate_val_);

    // end
    return;
}

void ObstaclePublisher3d::readParameters()
{
    //
    ros::param::param<std::string>("~obstacles_file", obstacles_file_, "obstacles_file.xml");
    ROS_INFO("obstacles_file = %s",obstacles_file_.c_str());

    //
    ros::param::param<std::string>("~obstacles_pub_topic_name", obstacles_pub_topic_name_, "obstacles");
    ROS_INFO("obstacles_pub_topic_name = %s",obstacles_pub_topic_name_.c_str());

    //
    ros::param::param<double>("~rate_val", rate_val_, 10.0);
    ROS_INFO("rate_val = %f",rate_val_);

    //
    ros::param::param<std::string>("~world_ref_frame", world_ref_frame_, "world");
    ROS_INFO("world_ref_frame = %s",world_ref_frame_.c_str());


    return;
}

int ObstaclePublisher3d::readConfigFile()
{
    //XML document
    pugi::xml_document doc;
    std::ifstream nameFile(obstacles_file_.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if (!result) //FIXME, pass as argument
    {
        std::cout << "Xml file missing " << std::endl;
        assert(0);
    }

    ///// Obstacles
    pugi::xml_node obstacles = doc.child("map").child("objects");

    // Clear lists

    // Obstacles 3d
    obstacles_pub_msg_.markers.clear();


    // Aux
    std::string readingValue;

    //Obstacles variables!
    int id_object;
    std::vector<double> attitude_object(4);
    std::vector<double> center_point_object(3);
    // Ellipsoid
    std::vector<double> radii_ellipsoid(3);
    // Cylinder
    std::vector<double> radii_cylinder(2);
    double height_cylinder;
    // Cube
    std::vector<double> size_cube(3);


    //Reading ellipsoids
    for(pugi::xml_node ellipsoid_xml = obstacles.child("ellipsoid"); ellipsoid_xml; ellipsoid_xml = ellipsoid_xml.next_sibling("ellipsoid"))
    {
        //Id obstacle
        readingValue=ellipsoid_xml.child_value("id");
        std::istringstream convertId(readingValue);
        convertId>>id_object;
        std::cout<<"-Added ellipsoid id="<<id_object;

        //Center point
        readingValue=ellipsoid_xml.child_value("center_point");
        std::istringstream convertCenterPoint(readingValue);
        for(unsigned int i=0;i<3;i++)
        {
            convertCenterPoint>>center_point_object[i];
        }
        std::cout<<"; center_point=[ "<<center_point_object[0]<<", "<<center_point_object[1]<<", "<<center_point_object[2]<<" ]";

        // Attitude
        readingValue=ellipsoid_xml.child_value("attitude");
        std::istringstream convertAttitude(readingValue);
        for(unsigned int i=0;i<4;i++)
        {
            convertAttitude>>attitude_object[i];
        }
        std::cout<<"; attitude=[ "<<attitude_object[0]<<", "<<attitude_object[1]<<", "<<attitude_object[2]<<", "<<attitude_object[3]<<" ]";

        //Radius
        readingValue=ellipsoid_xml.child_value("radii");
        std::istringstream convertSize(readingValue);
        for(unsigned int i=0;i<3;i++)
        {
            convertSize>>radii_ellipsoid[i];
        }
        std::cout<<"; radii=[ "<<radii_ellipsoid[0]<<", "<<radii_ellipsoid[1]<<", "<<radii_ellipsoid[2]<<" ]";



        // Add as obstacle 3d
        visualization_msgs::Marker marker;

        marker.header.stamp=ros::Time::now();
        marker.header.frame_id=world_ref_frame_;
        marker.ns="ellipsoid_"+std::to_string(id_object);
        marker.id=id_object;
        marker.action=visualization_msgs::Marker::ADD;
        marker.type=visualization_msgs::Marker::SPHERE;
        marker.lifetime=ros::Duration(0);
        marker.frame_locked=true;
        marker.color.r=0.8;
        marker.color.g=0.8;
        marker.color.b=0.8;
        marker.color.a=0.9;
        marker.pose.position.x=center_point_object[0];
        marker.pose.position.y=center_point_object[1];
        marker.pose.position.z=center_point_object[2];
        marker.pose.orientation.w=attitude_object[0];
        marker.pose.orientation.x=attitude_object[1];
        marker.pose.orientation.y=attitude_object[2];
        marker.pose.orientation.z=attitude_object[3];
        marker.scale.x=2.0*radii_ellipsoid[0];
        marker.scale.y=2.0*radii_ellipsoid[1];
        marker.scale.z=2.0*radii_ellipsoid[2];

        // Push
        obstacles_pub_msg_.markers.push_back(marker);


        // End object
        std::cout<<std::endl;
    }



    //Reading cylinders
    for(pugi::xml_node cylinder_xml = obstacles.child("cylinder"); cylinder_xml; cylinder_xml = cylinder_xml.next_sibling("cylinder"))
    {
        //Id obstacle
        readingValue=cylinder_xml.child_value("id");
        std::istringstream convertId(readingValue);
        convertId>>id_object;
        std::cout<<"-Added cylinder id="<<id_object;

        //Center point
        readingValue=cylinder_xml.child_value("center_point");
        std::istringstream convertCenterPoint(readingValue);
        for(unsigned int i=0;i<3;i++)
        {
            convertCenterPoint>>center_point_object[i];
        }
        std::cout<<"; center_point=[ "<<center_point_object[0]<<", "<<center_point_object[1]<<", "<<center_point_object[2]<<" ]";

        // Attitude
        readingValue=cylinder_xml.child_value("attitude");
        std::istringstream convertAttitude(readingValue);
        for(unsigned int i=0;i<4;i++)
        {
            convertAttitude>>attitude_object[i];
        }
        std::cout<<"; attitude=[ "<<attitude_object[0]<<", "<<attitude_object[1]<<", "<<attitude_object[2]<<", "<<attitude_object[3]<<" ]";

        //Radius
        readingValue=cylinder_xml.child_value("radii");
        std::istringstream convertSize(readingValue);
        for(unsigned int i=0;i<2;i++)
        {
            convertSize>>radii_cylinder[i];
        }
        std::cout<<"; radii=[ "<<radii_cylinder[0]<<", "<<radii_cylinder[1]<<" ]";

        //Height
        readingValue=cylinder_xml.child_value("height");
        std::istringstream convertHeight(readingValue);
        convertHeight>>height_cylinder;
        std::cout<<"; height=[ "<<height_cylinder<<" ]";



        // Add as obstacle 3d
        visualization_msgs::Marker marker;

        marker.header.stamp=ros::Time::now();
        marker.header.frame_id=world_ref_frame_;
        marker.ns="cylinder_"+std::to_string(id_object);
        marker.id=id_object;
        marker.action=visualization_msgs::Marker::ADD;
        marker.type=visualization_msgs::Marker::CYLINDER;
        marker.lifetime=ros::Duration(0);
        marker.frame_locked=true;
        marker.color.r=0.8;
        marker.color.g=0.8;
        marker.color.b=0.8;
        marker.color.a=0.9;
        marker.pose.position.x=center_point_object[0];
        marker.pose.position.y=center_point_object[1];
        marker.pose.position.z=center_point_object[2];
        marker.pose.orientation.w=attitude_object[0];
        marker.pose.orientation.x=attitude_object[1];
        marker.pose.orientation.y=attitude_object[2];
        marker.pose.orientation.z=attitude_object[3];
        marker.scale.x=2.0*radii_cylinder[0];
        marker.scale.y=2.0*radii_cylinder[1];
        marker.scale.z=height_cylinder;

        // Push
        obstacles_pub_msg_.markers.push_back(marker);


        // End object
        std::cout<<std::endl;
    }



    // Reading Cubes
    for(pugi::xml_node cube_xml = obstacles.child("cube"); cube_xml; cube_xml = cube_xml.next_sibling("cube"))
    {
        std::string readingValue;

        //Id obstacle
        readingValue=cube_xml.child_value("id");
        std::istringstream convertId(readingValue);
        convertId>>id_object;
        std::cout<<"-Added cube id="<<id_object;

        //Center point
        readingValue=cube_xml.child_value("center_point");
        std::istringstream convertCenterPoint(readingValue);
        for(unsigned int i=0;i<3;i++)
        {
            convertCenterPoint>>center_point_object[i];
        }
        std::cout<<"; center_point=[ "<<center_point_object[0]<<", "<<center_point_object[1]<<", "<<center_point_object[2]<<" ]";

        // Attitude
        readingValue=cube_xml.child_value("attitude");
        std::istringstream convertAttitude(readingValue);
        for(unsigned int i=0;i<4;i++)
        {
            convertAttitude>>attitude_object[i];
        }
        std::cout<<"; attitude=[ "<<attitude_object[0]<<", "<<attitude_object[1]<<", "<<attitude_object[2]<<", "<<attitude_object[3]<<" ]";

        //size
        readingValue=cube_xml.child_value("size");
        std::istringstream convertSize(readingValue);
        for(unsigned int i=0;i<3;i++)
        {
            convertSize>>size_cube[i];
        }
        std::cout<<"; sizes=[ "<<size_cube[0]<<", "<<size_cube[1]<<", "<<size_cube[2]<<" ]";


        // Add as obstacle 3d
        visualization_msgs::Marker marker;

        marker.header.stamp=ros::Time::now();
        marker.header.frame_id=world_ref_frame_;
        marker.ns="cube_"+std::to_string(id_object);
        marker.id=id_object;
        marker.action=visualization_msgs::Marker::ADD;
        marker.type=visualization_msgs::Marker::CUBE;
        marker.lifetime=ros::Duration(0);
        marker.frame_locked=true;
        marker.color.r=0.8;
        marker.color.g=0.8;
        marker.color.b=0.8;
        marker.color.a=0.9;
        marker.pose.position.x=center_point_object[0];
        marker.pose.position.y=center_point_object[1];
        marker.pose.position.z=center_point_object[2];
        marker.pose.orientation.w=attitude_object[0];
        marker.pose.orientation.x=attitude_object[1];
        marker.pose.orientation.y=attitude_object[2];
        marker.pose.orientation.z=attitude_object[3];
        marker.pose.orientation.z=0.0;
        marker.scale.x=size_cube[0];
        marker.scale.y=size_cube[1];
        marker.scale.z=size_cube[2];

        // Push
        obstacles_pub_msg_.markers.push_back(marker);



        // End object
        std::cout<<std::endl;

    }
    // End
    return 0;
}

int ObstaclePublisher3d::publish()
{
    obstacles_pub_.publish(obstacles_pub_msg_);

    return 0;
}

int ObstaclePublisher3d::open()
{
    // read config file
    readConfigFile();


    // Publishers
    obstacles_pub_ = nh_->advertise<visualization_msgs::MarkerArray>(obstacles_pub_topic_name_, 10);

    // end
    return 0;
}

void ObstaclePublisher3d::sleep()
{
    rate_->sleep();
    return;
}

int ObstaclePublisher3d::run()
{
    while(ros::ok())
    {
        // spin
        ros::spinOnce();

        // publish
        publish();

        // sleep
        sleep();
    }


    // finish
    return 0;
}





/////////////////// ObstaclePublisher2d

ObstaclePublisher2d::ObstaclePublisher2d(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    nh_=new ros::NodeHandle();

    // info
    ROS_INFO("Starting %s",ros::this_node::getName().c_str());

    // read parameters
    readParameters();

    // post initializations
    rate_=new ros::Rate(rate_val_);

    // end
    return;
}

void ObstaclePublisher2d::readParameters()
{
    //
    ros::param::param<std::string>("~obstacles_file", obstacles_file_, "obstacle_config.xml");
    ROS_INFO("obstacles_file = %s",obstacles_file_.c_str());

    //
    ros::param::param<std::string>("~obstacles_2d_pub_topic_name", obstacles_2d_pub_topic_name_, "obstacles_2d");
    ROS_INFO("obstacles_2d_pub_topic_name = %s",obstacles_2d_pub_topic_name_.c_str());
    //
    ros::param::param<std::string>("~obstacles_3d_pub_topic_name", obstacles_3d_pub_topic_name_, "obstacles_3d");
    ROS_INFO("obstacles_3d_pub_topic_name = %s",obstacles_3d_pub_topic_name_.c_str());

    //
    ros::param::param<double>("~rate_val", rate_val_, 10.0);
    ROS_INFO("rate_val = %f",rate_val_);

    //
    ros::param::param<std::string>("~world_ref_frame", world_ref_frame_, "world");
    ROS_INFO("world_ref_frame = %s",world_ref_frame_.c_str());


    return;
}

int ObstaclePublisher2d::readConfigFile()
{
    //XML document
    pugi::xml_document doc;
    std::ifstream nameFile(obstacles_file_.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if (!result) //FIXME, pass as argument
    {
        std::cout << "Xml file missing " << std::endl;
        assert(0);
    }

    ///// Obstacles
    pugi::xml_node obstacles = doc.child("map").child("objects");

    // Clear lists
    // Obstacles 2d
    obstacles_2d_pub_msg_.ellipses.clear();
    obstacles_2d_pub_msg_.rectangles.clear();

    // Obstacles 3d
    obstacles_3d_pub_msg_.markers.clear();


    // Aux
    std::string readingValue;

    //Obstacles variables!
    int id_object;
    double attitude_object;
    std::vector<double> center_point_object(2);
    //Ellipses
    std::vector<double> obstRadius(2);
    //Rectangles
    std::vector<double> obstSize(2);


    //Reading ellipses!!
    for(pugi::xml_node ellipse_xml = obstacles.child("ellipse"); ellipse_xml; ellipse_xml = ellipse_xml.next_sibling("ellipse"))
    {
        //Id obstacle
        readingValue=ellipse_xml.child_value("id");
        std::istringstream convertId(readingValue);
        convertId>>id_object;
        std::cout<<"-Added ellipse id="<<id_object;

        //Center point
        readingValue=ellipse_xml.child_value("center_point");
        std::istringstream convertCenterPoint(readingValue);
        for(unsigned int i=0;i<2;i++)
        {
            convertCenterPoint>>center_point_object[i];
        }
        std::cout<<"; center_point=[ "<<center_point_object[0]<<", "<<center_point_object[1]<<" ]";

        //Yaw Angle
        readingValue=ellipse_xml.child_value("yaw_angle");
        std::istringstream convertYawAngle(readingValue);
        convertYawAngle>>attitude_object;
        attitude_object*=M_PI/180.0;
        std::cout<<"; yaw_angle="<<attitude_object*180/M_PI;
        Eigen::Vector4d attitude_quat=Quaternion::eulerAnglesToQuaternion(attitude_object, 0, 0);

        //Radius
        readingValue=ellipse_xml.child_value("radii");
        std::istringstream convertSize(readingValue);
        for(unsigned int i=0;i<2;i++)
        {
            convertSize>>obstRadius[i];
        }
        std::cout<<"; radii=[ "<<obstRadius[0]<<", "<<obstRadius[1]<<" ]";



        // Add as obstacle 2d
        environment_msgs::Ellipse ellipse;
        ellipse.common.header.stamp=ros::Time::now();
        ellipse.common.header.frame_id=world_ref_frame_;
        ellipse.common.ns="ellipse_"+std::to_string(id_object);
        ellipse.common.id=id_object;
        ellipse.common.action=environment_msgs::Object2d::ADD;
        ellipse.common.lifetime=ros::Duration(0);
        ellipse.common.color.r=0.8;
        ellipse.common.color.g=0.8;
        ellipse.common.color.b=0.8;
        ellipse.common.color.a=0.9;
        ellipse.pose_center.position.x=center_point_object[0];
        ellipse.pose_center.position.y=center_point_object[1];
        ellipse.pose_center.position.z=0.0;
        ellipse.pose_center.orientation.w=attitude_quat[0];
        ellipse.pose_center.orientation.x=attitude_quat[1];
        ellipse.pose_center.orientation.y=attitude_quat[2];
        ellipse.pose_center.orientation.z=attitude_quat[3];
        ellipse.radius_x=obstRadius[0];
        ellipse.radius_y=obstRadius[1];

        // Push
        obstacles_2d_pub_msg_.ellipses.push_back(ellipse);



        // Add as obstacle 3d
        visualization_msgs::Marker marker;

        marker.header.stamp=ros::Time::now();
        marker.header.frame_id=world_ref_frame_;
        marker.ns="ellipse_"+std::to_string(id_object);
        marker.id=id_object;
        marker.action=visualization_msgs::Marker::ADD;
        marker.type=visualization_msgs::Marker::CYLINDER;
        marker.lifetime=ros::Duration(0);
        marker.frame_locked=true;
        marker.color.r=0.8;
        marker.color.g=0.8;
        marker.color.b=0.8;
        marker.color.a=0.9;
        marker.pose.position.x=center_point_object[0];
        marker.pose.position.y=center_point_object[1];
        marker.pose.position.z=0.0;
        marker.pose.orientation.w=attitude_quat[0];
        marker.pose.orientation.x=attitude_quat[1];
        marker.pose.orientation.y=attitude_quat[2];
        marker.pose.orientation.z=attitude_quat[3];
        marker.scale.x=2.0*obstRadius[0];
        marker.scale.y=2.0*obstRadius[1];
        marker.scale.z=10;

        // Push
        obstacles_3d_pub_msg_.markers.push_back(marker);


        // End object
        std::cout<<std::endl;
    }



    // Reading Rectangles
    for(pugi::xml_node rectangle_xml = obstacles.child("rectangle"); rectangle_xml; rectangle_xml = rectangle_xml.next_sibling("rectangle"))
    {
        std::string readingValue;

        //Id obstacle
        readingValue=rectangle_xml.child_value("id");
        std::istringstream convertId(readingValue);
        convertId>>id_object;
        std::cout<<"-Added rectangle id="<<id_object;

        //Center point
        readingValue=rectangle_xml.child_value("center_point");
        std::istringstream convertCenterPoint(readingValue);
        for(unsigned int i=0;i<2;i++)
        {
            convertCenterPoint>>center_point_object[i];
        }
        std::cout<<"; center_point=[ "<<center_point_object[0]<<", "<<center_point_object[1]<<" ]";

        //Yaw Angle
        readingValue=rectangle_xml.child_value("yaw_angle");
        std::istringstream convertYawAngle(readingValue);
        convertYawAngle>>attitude_object;
        attitude_object*=M_PI/180.0;
        std::cout<<"; yaw_angle="<<attitude_object*180/M_PI;
        Eigen::Vector4d attitude_quat=Quaternion::eulerAnglesToQuaternion(attitude_object, 0, 0);

        //size
        readingValue=rectangle_xml.child_value("size");
        std::istringstream convertSize(readingValue);
        for(unsigned int i=0;i<2;i++)
        {
            convertSize>>obstSize[i];
        }
        std::cout<<"; sizes=[ "<<obstSize[0]<<", "<<obstSize[1]<<" ]";



        // Add as obstacle 2d
        environment_msgs::Rectangle rectangle;
        rectangle.common.header.stamp=ros::Time::now();
        rectangle.common.header.frame_id=world_ref_frame_;
        rectangle.common.ns="rectangle_"+std::to_string(id_object);
        rectangle.common.id=id_object;
        rectangle.common.action=environment_msgs::Object2d::ADD;
        rectangle.common.lifetime=ros::Duration(0);
        rectangle.common.color.r=0.8;
        rectangle.common.color.g=0.8;
        rectangle.common.color.b=0.8;
        rectangle.common.color.a=0.9;
        rectangle.pose_center.position.x=center_point_object[0];
        rectangle.pose_center.position.y=center_point_object[1];
        rectangle.pose_center.position.z=0.0;
        rectangle.pose_center.orientation.w=attitude_quat[0];
        rectangle.pose_center.orientation.x=attitude_quat[1];
        rectangle.pose_center.orientation.y=attitude_quat[2];
        rectangle.pose_center.orientation.z=attitude_quat[3];
        rectangle.size_x=obstSize[0];
        rectangle.size_y=obstSize[1];

        // Push
        obstacles_2d_pub_msg_.rectangles.push_back(rectangle);



        // Add as obstacle 3d
        visualization_msgs::Marker marker;

        marker.header.stamp=ros::Time::now();
        marker.header.frame_id=world_ref_frame_;
        marker.ns="rectangle_"+std::to_string(id_object);
        marker.id=id_object;
        marker.action=visualization_msgs::Marker::ADD;
        marker.type=visualization_msgs::Marker::CUBE;
        marker.lifetime=ros::Duration(0);
        marker.frame_locked=true;
        marker.color.r=0.8;
        marker.color.g=0.8;
        marker.color.b=0.8;
        marker.color.a=0.9;
        marker.pose.position.x=center_point_object[0];
        marker.pose.position.y=center_point_object[1];
        marker.pose.position.z=0.0;
        marker.pose.orientation.w=attitude_quat[0];
        marker.pose.orientation.x=attitude_quat[1];
        marker.pose.orientation.y=attitude_quat[2];
        marker.pose.orientation.z=attitude_quat[3];
        marker.scale.x=obstSize[0];
        marker.scale.y=obstSize[1];
        marker.scale.z=10;

        // Push
        obstacles_3d_pub_msg_.markers.push_back(marker);



        // End object
        std::cout<<std::endl;

    }
    // End
    return 0;
}

int ObstaclePublisher2d::publish()
{
    obstacles_3d_pub_.publish(obstacles_3d_pub_msg_);
    obstacles_2d_pub_.publish(obstacles_2d_pub_msg_);

    return 0;
}

int ObstaclePublisher2d::open()
{
    // read config file
    readConfigFile();


    // Publishers
    obstacles_2d_pub_ = nh_->advertise<environment_msgs::Object2dArray>(obstacles_2d_pub_topic_name_, 10);

    obstacles_3d_pub_ = nh_->advertise<visualization_msgs::MarkerArray>(obstacles_3d_pub_topic_name_, 10);


    // end
    return 0;
}

void ObstaclePublisher2d::sleep()
{
    rate_->sleep();
    return;
}

int ObstaclePublisher2d::run()
{
    while(ros::ok())
    {
        // spin
        ros::spinOnce();

        // publish
        publish();

        // sleep
        sleep();
    }


    // finish
    return 0;
}
