

//I/O stream
//std::cout
#include <iostream>


//
#include "obstacle_publisher/obstacle_publisher.h"



int main(int argc,char **argv)
{
    ObstaclePublisher3d obstacle_publisher(argc, argv);

    obstacle_publisher.open();

    obstacle_publisher.run();

    return 0;
}
